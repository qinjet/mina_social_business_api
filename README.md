1、本项目分两个工程进行部署，部署打包的模块为： supplier-deploy和portal-deploy<br/>
2、具体依赖关系查看pom文件的dependency<br/>
3、supplier-api对外提供供应商管理的服务的接口<br/>
4、portal-api对外提供用户访问平台的接口<br/>
5、platform-common提供整个项目功能的框架功能<br/>
6、platform-pub提供功能业务功能<br/>
7、platform-cloud提供云服务功能<br/>

技术选型：<br/>
采用sprintboot作为基础框架<br/>
阿里云的oss作为存储<br/>
短信的下发为阿里云的短信服务<br/>
快递查询是kuai100提供服务<br/>
idCard识别也是由阿里云提供<br/>
系统的缓存使用了redis<br/>



<img alt="系统截图" width="200" height="290" src="https://gitee.com/liuhangjun/mina_social_business/attach_files/download?i=166133&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F04%2FDA%2FPaAvDFuT97iAWuWmAAJZIYMwJ1896.jpeg%3Ftoken%3D6c5c0a9cf04e37cce8ccc6322664835e%26ts%3D1536424127%26attname%3D1.jpeg">
<img alt="系统截图" width="200" height="290" src="https://gitee.com/liuhangjun/mina_social_business/attach_files/download?i=166138&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F04%2FDB%2FPaAvDFuT-fCAeNp7AAH99HxJhZA64.jpeg%3Ftoken%3Db042424b1872a2982959f83f2a8e6e46%26ts%3D1536424867%26attname%3D2.jpeg">
<img alt="系统截图" width="200" height="290" src="https://gitee.com/liuhangjun/mina_social_business/attach_files/download?i=166139&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F04%2FDB%2FPaAvDFuT-iKAETJ7AANQP-FiU2g640.png%3Ftoken%3D682060c74feb90a1274d89a1cae2f205%26ts%3D1536424482%26attname%3D3.png">
<img alt="系统截图" width="200" height="290" src="https://gitee.com/liuhangjun/mina_social_business/attach_files/download?i=166137&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F04%2FDA%2FPaAvDFuT9_WAOeQNAAFjducaSkI09.jpeg%3Ftoken%3Dd5be6fed9576ef2ef099ad219cdc8c8f%26ts%3D1536424127%26attname%3D4.jpeg">
